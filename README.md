# git-our-shit-together-test-repo

This is the toy repository for participants of the _Git our shit together_ workshops.

Slides can be found here: [https://frie.codes/page/gost/](https://frie.codes/page/gost/).

If it is not working in Windows, try "ssh -T git@gitlab.com" in GitBash. For first time usage you need to change user.name and user.email in GitBash for VSCode.

Link to repository with code for slides and installation instructions: [https://gitlab.com/friep/git-our-shit-together](https://gitlab.com/friep/git-our-shit-together)

## Markdown syntax

- **bold**
- _italic_
- ~~strike~~
### subheader

> this is a quote

this links an image from the images folder

![description](images/xkcd_comic.png)

image from the web

![description](https://imgs.xkcd.com/comics/correlation.png)
